﻿#include <iostream>
using namespace std;

class Animal
{
private:
    string phrase = "Empty";
public:
    Animal() {}
    virtual void Voice()
    {
        cout << phrase << endl;
    }
};

class Dog : public Animal
{
private:
    string phrase = "Woof!";
public:
    Dog() {}
    void Voice() override
    {
        cout << phrase << endl;
    }
};

class Cat : public Animal
{
private:
    string phrase = "Meow!";
public:
    Cat() {}
    void Voice() override
    {
        cout << phrase << endl;
    }
};

class Cow : public Animal
{
private:
    string phrase = "Moo!";
public:
    Cow() {}
    void Voice() override
    {
        cout << phrase << endl;
    }
};

int main()
{
    Animal *animals[3];
    Animal *dog = new Dog;
    Animal *cat = new Cat;
    Animal *cow = new Cow;
    animals[0] = dog;
    animals[1] = cat;
    animals[2] = cow;

    for (int i = 0; i < sizeof(animals); i++)
    {
        animals[i]->Voice();
    }

    return 0;
}